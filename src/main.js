import Vue from 'vue'
import App from './App.vue'
import LoadScript from 'vue-plugin-load-script';
import { MdButton, MdIcon} from 'vue-material/dist/components';
import 'vue-material/dist/vue-material.min.css';
import 'vue-material/dist/theme/default.css';
import VueApexCharts from 'vue-apexcharts';

Vue.component('apexchart', VueApexCharts);
Vue.config.productionTip = false
Vue.use(LoadScript);
Vue.use(MdButton);
Vue.use(MdIcon);

new Vue({
  render: h => h(App),
}).$mount('#app')
